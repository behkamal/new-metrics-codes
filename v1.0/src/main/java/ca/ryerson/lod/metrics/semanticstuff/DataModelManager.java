package ca.ryerson.lod.metrics.semanticstuff;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import ca.ryerson.lod.metrics.semanticstuff.rdfpersistance.DataSets;
import ca.ryerson.lod.metrics.semanticstuff.rdfpersistance.DataSets.DataSetType;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

/**
 * @author Zoran Jeremic 2013-07-16
 */
public class DataModelManager {

    private static class DataModelManagerHolder {

        private static final DataModelManager INSTANCE = new DataModelManager();
    }

    public static DataModelManager getInstance() {
        return DataModelManagerHolder.INSTANCE;
    }

    protected DataModelManager() {
    }

    public Model getDataModel(DataSetType dataSet) {
        Model model = ModelFactory.createDefaultModel();
        List<String> files = DataSets.getInstance().getDataSetPaths(dataSet);
        for (String path : files) {
            try {
                DataModelManager.getInstance().importFileToModel(model, path);

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return model;
    }

    public OntModel getOntoDataModel(DataSetType dataSet) {
        OntModel onto = ModelFactory.createOntologyModel(
                OntModelSpec.OWL_MEM, getDataModel(dataSet));

        return onto;
    }

    void importFileToModel(Model m, String path) throws FileNotFoundException, IOException {
        //path="data/geo/geopolitical_data.rdf";
        InputStream in = new FileInputStream(path);
        m.read(in, "");
        in.close();
    }
}
