package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.List;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Majid Sazvar
 */
/**
 *
 * Metric 25. Unused properties (properties defined in the schema but not
 * used in the dataset)
 *
 */
public class MetricTwentyFiveService extends AbstractMetricsService {

    private static final Logger logger = Logger.getLogger(MetricTwentyFiveService.class);
    private List<OntProperty> propertyList;

    @Override
    public ServiceResults getMetric(OntModel ontModel) {

        propertyList = ontModel.listAllOntProperties().toList();
        int propertyCount = propertyList.size();

        ServiceResults counter = super.getMetric(ontModel);

        //System.out.println(propertyList);
        
        counter.setDenominator(propertyCount);
        counter.setHitsCounter(propertyList.size());

        return counter;

    }

    @Override
    ServiceResults checkClassInstance(OntClass ontClass,
            ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {

        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());

        List<Statement> statements = ontResource.listProperties().toList();
        
        //System.out.println(classList);
        for (Statement statement : statements) {
            Property property = statement.getPredicate().as(Property.class);

            propertyList.remove(property);

        }
        
        return counter;
    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}
