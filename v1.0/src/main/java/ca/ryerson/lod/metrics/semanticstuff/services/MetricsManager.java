package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import ca.ryerson.lod.metrics.semanticstuff.DataModelManager;

import ca.ryerson.lod.metrics.semanticstuff.rdfpersistance.DataSets.DataSetType;
import ca.ryerson.lod.metrics.semanticstuff.services.ModelStatistics.ModelStats;
import ca.ryerson.lod.metrics.utility.CSVUtility;
import ca.ryerson.lod.metrics.utility.StringUtility;


import com.hp.hpl.jena.ontology.OntModel;
import sun.security.util.DerEncoder;

/**
 * @author Zoran Jeremic 2013-07-17
 */
public class MetricsManager {

    private static final Logger logger = Logger.getLogger(MetricsManager.class);
    MetricOneService m1Service = new MetricOneService();
    MetricTwoService m2Service = new MetricTwoService();
    MetricSixService m6Service = new MetricSixService();
    MetricEightService m8Service = new MetricEightService();
    MetricEighteenService m18Service = new MetricEighteenService();
    MetricTwentyService m20Service = new MetricTwentyService();
    MetricSevenService m7Service = new MetricSevenService();
    MetricTwentyOneService m21Service = new MetricTwentyOneService();
    MetricFifteenService m15Service = new MetricFifteenService();
    MetricFiveService m5Service = new MetricFiveService();
    MetricThreeService m3Service = new MetricThreeService();
    MetricElevenService m11Service = new MetricElevenService();
    MetricThirteenService m13Service = new MetricThirteenService();
    MetricFourteenService m14Service = new MetricFourteenService();
    MetricTwelveService m12Service = new MetricTwelveService();
    MetricSixteenService m16Service = new MetricSixteenService();
    MetricSeventeenService m17Service = new MetricSeventeenService();
    MetricNineteenService m19Service = new MetricNineteenService();
    //New metrics
    MetricTwentyTwoService m22Service = new MetricTwentyTwoService();
    MetricTwentyThreeService m23Service = new MetricTwentyThreeService();
    MetricTwentyFourService m24Service = new MetricTwentyFourService();
    MetricTwentyFiveService m25Service = new MetricTwentyFiveService();
    ModelStatistics modelStats = new ModelStatistics();

    public DataSetResults runMetricsOnDataset(DataSetType dataSet) {
        logger.info("Run metrics on dataset:" + dataSet.name());
        OntModel onto = DataModelManager.getInstance().getOntoDataModel(dataSet);
        DataSetResults dsResults = new DataSetResults();
        dsResults.setDataset(dataSet);

        ModelStats stats = modelStats.getModelStats(onto);
        dsResults.setModelStats(stats);

        Double m1 = m1Service.calculateMetric(onto);
        dsResults.setM1(m1);
        
        Double m2 = m2Service.calculateMetric(onto);
        dsResults.setM2(m2);
        
        Double m3 = m3Service.calculateMetric(onto);
        dsResults.setM3(m3);
        
        Double m22 = m22Service.calculateMetric(onto);
        dsResults.setM22(m22);
        
        Double m15 = m15Service.calculateMetric(onto);
        dsResults.setM15(m15);
        
        Double m19 = m19Service.calculateMetric(onto);
        dsResults.setM19(m19);
        
        Double m14 = m14Service.calculateMetric(onto);
        dsResults.setM14(m14);
        
        Double m16 = m16Service.calculateMetric(onto);
        dsResults.setM16(m16);
        
        Double m6 = m6Service.calculateMetric(onto);
        dsResults.setM6(m6);
        
        Double m11 = m11Service.calculateMetric(onto);
        dsResults.setM11(m11);
        
        Double m7 = m7Service.calculateMetric(onto);
        dsResults.setM7(m7);
        
        Double m23 = m23Service.calculateMetric(onto);
        dsResults.setM23(m23);
        
        Double m24 = m24Service.calculateMetric(onto);
        dsResults.setM24(m24);
        
        Double m25 = m25Service.calculateMetric(onto);
        dsResults.setM25(m25);

        return dsResults;
    }

    public DataSetResults runOneMetricOnDataset(DataSetType dataset, AbstractMetricsService mService) {
        System.out.println("dataset to run one metric:" + dataset.name());
        logger.info("Run one metrics on dataset :" + dataset.name());
        OntModel onto = DataModelManager.getInstance().getOntoDataModel(dataset);
        DataSetResults dsResults = new DataSetResults();
        dsResults.setDataset(dataset);
        Double m = mService.calculateMetric(onto);
        System.out.println("RESULT:" + m);
        dsResults.setM1(m);
        return dsResults;
    }

    public boolean runBasedOnInput(String choices) {
        System.out.println("runBased on input:" + choices);
        List<String> choicesList = StringUtility.getCommaSeparatedValuesAsList(choices);
        List<DataSetType> datasets = new LinkedList<DataSetType>();
        for (String choice : choicesList) {
            System.out.println("choice:" + choice);
            try {
                DataSetType dataSet = DataSetType.valueOf(choice);
                datasets.add(dataSet);

            } catch (java.lang.IllegalArgumentException illArgEx) {
                System.out.println("Dataset:" + choice + " doesn't exist. Please try again.");
                return false;
            }
        }
        runMetricsForDatasets(datasets);
        return true;
    }

    public void runMetricsForDatasets(List<DataSetType> datasets) {
        long time = System.currentTimeMillis();
        String path = String.valueOf(time) + ".csv";
        CSVUtility.createHeadings(path);
        for (DataSetType dataset : datasets) {
            logger.info(("run metrics for dataset:" + dataset.name()));
            DataSetResults dsResults = runMetricsOnDataset(dataset);
            logger.info(("finished metrics for dataset:" + dataset.name()));
            CSVUtility.appendDataToCSV(dsResults, path);
        }
    }

    public void runAllMetrics() {
        DataSetResults dsResults = runMetricsOnDataset(DataSetType.DS1);
        long time = System.currentTimeMillis();
        String path = String.valueOf(time) + ".csv";
        CSVUtility.createHeadings(path);
        CSVUtility.appendDataToCSV(dsResults, path);
    }
}
