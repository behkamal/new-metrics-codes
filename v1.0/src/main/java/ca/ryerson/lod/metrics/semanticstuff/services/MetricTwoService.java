package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 *
 * Metric 2. % triples with out-of-range data values 1-( The number of triples
 * contain properties with outlier values with respect to the ranges of
 * acceptable values defined in the schema / total numbers of triples in a
 * dataset)
 *
 */
public class MetricTwoService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MetricTwoService.class);

    @SuppressWarnings("unchecked")
    @Override
    ServiceResults checkClassInstance(OntClass ontClass, ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {

        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }        
        checkedInstances.add(ontResource.getURI());
        HashMap<String, OntResource> ranges = new HashMap<String, OntResource>();
        while (classproperties.hasNext()) {
            OntProperty subproperty = classproperties.next();
            OntResource range = subproperty.getRange();

            if (range != null) {
                //System.out.println(subproperty.getURI() + " - " + range);
                ranges.put(subproperty.getURI(), range);
            }
        }
        StmtIterator hasproperties = ontResource.listProperties();
        //List<String> hasPropertiesList=new LinkedList<String>();
        while (hasproperties.hasNext()) {
            boolean notmatch = false;
            Statement statement = hasproperties.next();
            Property p = statement.getPredicate();
            RDFNode object = statement.getObject();

            OntResource range = ranges.get(p.getURI());
            String datatypeUri = null;

            if (object.isLiteral()) {
                RDFDatatype datatype = object.asLiteral().getDatatype();

                if (datatype != null) {
                    datatypeUri = datatype.getURI();
                    if (range != null && range.getURI().equals(datatypeUri)) {
                        //System.out.println("MATCHING RANGE:" + range.getURI());
                    } else {
                        if (range != null) {
                            System.out.println("BUT SHOULD BE FROM RANGE:" + range.getURI());
                            notmatch = true;
                        }

                    }
                } else {
                    //System.out.println(object.asLiteral());
                    notmatch = true;
                }
            } else if (object.isURIResource()) {

                if (range != null && range.isClass()) {

                    OntClass rangeClass = range.asClass();
                    List<OntResource> instances = (List<OntResource>) rangeClass.listInstances().toList();
                    if (!instances.contains(object)) {

                        notmatch = true;
                    }
                }
            }
            counter.increaseDenominator();
            if (notmatch) {

                counter.increaseHits();
            }

        }

        return counter;

    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}
