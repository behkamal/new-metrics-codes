package ca.ryerson.lod.metrics.semanticstuff.services;

import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * Metric 5. % syntactically incorrect triples
 *1-(The number of triples contain properties with 
 *syntactic error  /  total numbers of triples in a dataset )
 */
public class MetricFiveService extends AbstractMetricsService{
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(MetricFiveService.class);
	@Override
	public ServiceResults getMetric(OntModel ontModel){
		ServiceResults counter=new ServiceResults();

	
		return counter;
	}

	@Override
	ServiceResults checkClassInstance(OntClass ontClass,
			ExtendedIterator<OntProperty> classproperties,
			OntResource ontResource, ServiceResults counter) {
		if(checkedInstances.contains(ontResource.getURI())){
			return counter;
		}
		checkedInstances.add(ontResource.getURI());
//		while(classproperties.hasNext()){
//			OntProperty property=classproperties.next();
//			
//		}
		return counter;
	}
	@Override
	Double calculateMetric(OntModel ontModel) {
		Double result=super.calculateMetric(ontModel);
		return (1.00-result);
	}

}
