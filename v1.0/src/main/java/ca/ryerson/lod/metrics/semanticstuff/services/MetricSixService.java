package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * Metric 6. % triples with improper assignments of data types to literals
 * 1-(The number of triples contain properties with improper assignment of data
 * type to literal / total numbers of triples in a dataset )
 *
 */
public class MetricSixService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MetricSixService.class);

    @Override
    ServiceResults checkClassInstance(OntClass ontClass, ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());
        HashMap<String, OntResource> ranges = new HashMap<String, OntResource>();
        //counter.increaseDenominator();
        while (classproperties.hasNext()) {
            OntProperty subproperty = classproperties.next();
            OntResource range = subproperty.getRange();

            if (range != null) {
                ranges.put(subproperty.getURI(), range);
            }
        }
        
        StmtIterator hasproperties = ontResource.listProperties();
        while (hasproperties.hasNext()) {
            boolean notmatch = false;
            Statement statement = hasproperties.next();
            //Resource s=statement.getSubject();
            Property p = statement.getPredicate();
            RDFNode object = statement.getObject();

            OntResource range = ranges.get(p.getURI());
            String datatypeUri = null;

            if (object.isLiteral()) {
                RDFDatatype datatype = object.asLiteral().getDatatype();

                if (datatype != null) {
                    datatypeUri = datatype.getURI();
                    if (range != null && range.getURI().equals(datatypeUri)) {
                        // System.out.println("MATCHING RANGE:"+range.getURI());
                    } else {
                        if (range != null) {
                            //System.out.println("BUT SHOULD BE FROM RANGE:"+range.getURI());
                            notmatch = true;
                        } else {
                            // System.out.println("RANGE WAS NOT DEFINED");
                        }

                    }
                } else {
                    if (range != null) {
                        // System.out.println("DATATYPE WAS NOT SPECIFIED");
                        notmatch = true;
                    }
                }
            }
            counter.increaseDenominator();
            if (notmatch) {
                counter.increaseHits();
            }

        }
        return counter;

    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}
