package ca.ryerson.lod.metrics.semanticstuff.services;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntModel;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * 
 * Metric 21. Number of missing properties
 * The number of missing properties with respect to properties defined in the schema
 *
 */
public class MetricTwentyOneService extends MetricOneService{
	private static final Logger logger = Logger.getLogger(MetricTwentyOneService.class);
	@Override
	Double calculateMetric(OntModel ontModel) {
		// TODO Auto-generated method stub
		ServiceResults counter=this.getMetric(ontModel);
		Double result=Double.valueOf(counter.getHitsCounter());
		logger.info(this.getClass().getSimpleName()+" RESULT:"+result+"  hits:"+counter.getHitsCounter()+"/"+counter.getDenominator());
		cleanData();
		return result;
	}
	
}
