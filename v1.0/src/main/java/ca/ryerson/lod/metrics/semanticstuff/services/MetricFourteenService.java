package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.FunctionalProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * Metric 14. Ratio of properties with redundant values 1-(The number of
 * properties contains redundant values / total numbers of properties defined in
 * the schema of a dataset )
 *
 */
public class MetricFourteenService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger
            .getLogger(MetricFourteenService.class);

    @Override
    ServiceResults checkOntClass(OntClass ontClass, ServiceResults counter) {
        if (checkedClasses.contains(ontClass.getURI())) {
            return counter;
        }
        checkedClasses.add(ontClass.getURI());
        @SuppressWarnings("unchecked")
        ExtendedIterator<OntResource> classInstances = (ExtendedIterator<OntResource>) ontClass
                .listInstances();
        ExtendedIterator<OntProperty> classproperties = ontClass
                .listDeclaredProperties();

        ArrayList<FunctionalProperty> functProperties = getAllFunctionalPropertiesOfClass(
                ontClass, classproperties);

        while (classInstances.hasNext()) {
            OntResource ontResource = classInstances.next();
            counter = checkClassInstance(ontClass, functProperties,
                    ontResource, counter);

        }
        return counter;
    }

    public ArrayList<FunctionalProperty> getAllFunctionalPropertiesOfClass(
            OntClass oC, ExtendedIterator<OntProperty> classproperties) {
        ArrayList<FunctionalProperty> dP = new ArrayList<FunctionalProperty>();
        while (classproperties.hasNext()) {
            OntProperty ontProp = classproperties.next();
            if (ontProp.isFunctionalProperty()) {
                dP.add(ontProp.asFunctionalProperty());
            }
        }

        return dP;
    }

    ServiceResults checkClassInstance(OntClass ontClass,
            ArrayList<FunctionalProperty> functProperties,
            OntResource ontResource, ServiceResults counter) {
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());
        List<Statement> properties = ontResource.listProperties().toList();
        if (properties != null && properties.size() > 0) {
            counter.increaseDenominator(properties.size());
        }
        for (FunctionalProperty fP : functProperties) {

            //System.out.println("One FunctionalProperty found!");

            if (ontResource.hasProperty(fP)) {
                int numberOfPropInstances = 0;
                Property fPredicate = ontResource.getProperty(fP)
                        .getPredicate();
                for (Statement prop : properties) {
                    if (prop.getPredicate().getURI()
                            .equals(fPredicate.getURI())) {
                        numberOfPropInstances++;
                    }
                }
                if (numberOfPropInstances > 1) {
                    counter.increaseHits();
                }
            }
        }

        return counter;
    }

    @Override
    ServiceResults checkClassInstance(OntClass ontClass,
            ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {

        return null;
    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}
