package ca.ryerson.lod.metrics.semanticstuff.services;

import com.hp.hpl.jena.ontology.Individual;
import java.util.List;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-28
 */
public class ModelStatistics {

    public ModelStats getModelStats(OntModel onto) {
        ModelStats stats = new ModelStats();

        List<OntClass> classes = onto.listNamedClasses().toList();

        //System.out.println(classes.toString());

        stats.classesNo = classes.size();
        ExtendedIterator<Individual> individuals = onto.listIndividuals();
         
        int numberOfInstances = 0;
        int numberOfTriples = 0;
        while (individuals.hasNext()) {
            
            Individual instance = individuals.next();
            
            numberOfInstances++;

            int triplesSize = instance.listProperties().toList().size();
            numberOfTriples = numberOfTriples + triplesSize;

        }

        stats.instancesNo = numberOfInstances;
        int numbOfProperties = onto.listAllOntProperties().toList().size();
        stats.propertiesNo = numbOfProperties;
        stats.triplesNo = numberOfTriples;


        return stats;
    }

    public class ModelStats {

        public int classesNo;
        public int instancesNo;
        public int propertiesNo;
        public int triplesNo;
    }
}
