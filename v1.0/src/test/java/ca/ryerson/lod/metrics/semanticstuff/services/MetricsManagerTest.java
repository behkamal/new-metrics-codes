package ca.ryerson.lod.metrics.semanticstuff.services;

import org.junit.Test;

import ca.ryerson.lod.metrics.semanticstuff.rdfpersistance.DataSets.DataSetType;

/**
 * @author Zoran Jeremic 2013-07-17
 */
public class MetricsManagerTest {

    @Test
    public void runOneMetricTest() {
        DataSetType dataset = DataSetType.TDS1;
        System.out.println("RUN THIS TEST:" + dataset.name());
        MetricsManager mm = new MetricsManager();
        //mm.runAllMetrics();
        // MetricThirteenService m13Service=new MetricThirteenService();
        MetricSevenService mService = new MetricSevenService();
        mm.runOneMetricOnDataset(dataset, mService);

    }
}
