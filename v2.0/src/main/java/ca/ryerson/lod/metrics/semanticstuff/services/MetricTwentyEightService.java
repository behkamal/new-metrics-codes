package ca.ryerson.lod.metrics.semanticstuff.services;

import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.ObjectProperty;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.util.ArrayList;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 28. % The ratio of invalid usage of data/object properties: 1-(The
 * total number of invalid usage of properties / total numbers of triples)
 *
 */
public class MetricTwentyEightService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MetricTwentyEightService.class);
    private ArrayList<String> dataPropertiesList = new ArrayList<>();
    private ArrayList<String> objectPropertiesList = new ArrayList<>();

    @Override
    public ServiceResults getMetric(OntModel ontModel) {

        ExtendedIterator<DatatypeProperty> dIterator = ontModel.listDatatypeProperties();

        while (dIterator.hasNext()) {
            DatatypeProperty dProperty = dIterator.next();
            dataPropertiesList.add(dProperty.getURI());
        }

        ExtendedIterator<ObjectProperty> oIterator = ontModel.listObjectProperties();

        while (oIterator.hasNext()) {
            ObjectProperty oProperty = oIterator.next();
            objectPropertiesList.add(oProperty.getURI());
        }

        //System.out.println(dataPropertiesList.toString());
        //System.out.println(objectPropertiesList.toString());

        ServiceResults counter = super.getMetric(ontModel);

        return counter;

    }

    @Override
    ServiceResults checkClassInstance(OntClass ontClass, ExtendedIterator<OntProperty> classproperties, OntResource ontResource,
            ServiceResults counter) {
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());
        StmtIterator hasproperties = ontResource.listProperties();

        while (hasproperties.hasNext()) {

            counter.increaseDenominator();

            Statement statement = hasproperties.next();
            Property property = statement.getPredicate();
            RDFNode object = statement.getObject();

            if (dataPropertiesList.contains(property.getURI())) {

                if (object.isResource()) {
                    counter.increaseHits();
                }

            } else if (objectPropertiesList.contains(property.getURI())) {

                if (object.isLiteral()) {
                    counter.increaseHits();
                }

            }

        }

        return counter;
    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);

    }
}
