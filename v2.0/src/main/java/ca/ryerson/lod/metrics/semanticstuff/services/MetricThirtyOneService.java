package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.logging.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 31. The number of properties defined in schema / total numbers of
 * classes defined in schema
 *
 */
public class MetricThirtyOneService extends AbstractMetricsService {

    private static Logger logger = Logger.getLogger(MetricThirtyOneService.class
            .getName());

    @Override
    ServiceResults checkClassInstance(OntClass ontClass,
            ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {

        return null;
    }

    @Override
    public ServiceResults getMetric(OntModel ontModel) {
        ServiceResults counter = new ServiceResults();

        int classesCount = ontModel.listNamedClasses().toList().size();
        int propertiesCount = ontModel.listAllOntProperties().toList().size();

        counter.setHitsCounter(propertiesCount);
        counter.setDenominator(classesCount);

        return counter;
    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return result;
    }
}
