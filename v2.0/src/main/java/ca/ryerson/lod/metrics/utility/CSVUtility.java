package ca.ryerson.lod.metrics.utility;

import java.io.FileWriter;
import java.io.IOException;

import ca.ryerson.lod.metrics.semanticstuff.services.DataSetResults;

/**
 * @author Zoran Jeremic 2013-07-25
 */
public class CSVUtility {

    static String root = "data/output/";

    public static void createHeadings(String filename) {
        String path = root + filename;
        FileWriter pw;
        try {
            pw = new FileWriter(path, true);

            pw.append("dataset");
            pw.append(",");
            pw.append("classesNo");
            pw.append(",");
            pw.append("instancesNo");
            pw.append(",");
            pw.append("propertiesNo");
            pw.append(",");
            pw.append("triplesNo");
            pw.append(",");
            pw.append("m1");
            pw.append(",");
            pw.append("m2");
            pw.append(",");
            pw.append("m3");
            pw.append(",");
            pw.append("m4");
            pw.append(",");
            pw.append("m5");
            pw.append(",");
            pw.append("m6");
            pw.append(",");
            pw.append("m7");
            pw.append(",");
            pw.append("m8");
            pw.append(",");
            pw.append("m9");
            pw.append(",");
            pw.append("m10");
            pw.append(",");
            pw.append("m11");
            pw.append(",");
            pw.append("m12");
            pw.append(",");
            pw.append("m13");
            pw.append(",");
            pw.append("m14");            
            pw.append(",");
            pw.append("m15");
            pw.append(",");
            pw.append("m16");            
            pw.append(",");
            pw.append("m17");
            pw.append(",");
            pw.append("m18");
            pw.append(",");
            pw.append("m19");            
            pw.append(",");
            pw.append("m20");
            pw.append(",");
            pw.append("m21");            
            pw.append(",");
            pw.append("m22");            
            pw.append(",");
            pw.append("m23");            
            pw.append(",");
            pw.append("m24");            
            pw.append(",");
            pw.append("m25");            
            pw.append(",");
            pw.append("m26");            
            pw.append(",");
            pw.append("m27");            
            pw.append(",");
            pw.append("m28");            
            pw.append(",");
            pw.append("m29");            
            pw.append(",");
            pw.append("m30");            
            pw.append(",");
            pw.append("m31");            
            pw.append(",");
            pw.append("m32");            
            
            pw.append("\n");
            pw.flush();
            pw.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static void appendToFileWriter(FileWriter pw, Double value) {
        if (value == null) {
            try {
                pw.append("N/A");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            try {
                pw.append(value.toString());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public static void appendDataToCSV(DataSetResults ds, String filename) {
        String path = root + filename;
        FileWriter pw;
        try {
            pw = new FileWriter(path, true);
            pw.append(ds.getDataset().name());
            pw.append(",");
            pw.append(String.valueOf(ds.getModelStats().classesNo));
            pw.append(",");
            pw.append(String.valueOf(ds.getModelStats().instancesNo));
            pw.append(",");
            pw.append(String.valueOf(ds.getModelStats().propertiesNo));
            pw.append(",");
            pw.append(String.valueOf(ds.getModelStats().triplesNo));
            pw.append(",");
            appendToFileWriter(pw, ds.getM1());
            pw.append(",");
            appendToFileWriter(pw, ds.getM2());
            pw.append(",");
            appendToFileWriter(pw, ds.getM3());
            pw.append(",");
            appendToFileWriter(pw, ds.getM4());
            pw.append(",");
            appendToFileWriter(pw, ds.getM5());
            pw.append(",");
            appendToFileWriter(pw, ds.getM6());
            pw.append(",");
            appendToFileWriter(pw, ds.getM7());
            pw.append(",");
            appendToFileWriter(pw, ds.getM8());
            pw.append(",");
            appendToFileWriter(pw, ds.getM9());
            pw.append(",");
            appendToFileWriter(pw, ds.getM10());
            pw.append(",");
            appendToFileWriter(pw, ds.getM11());
            pw.append(",");
            appendToFileWriter(pw, ds.getM12());
            pw.append(",");
            appendToFileWriter(pw, ds.getM13());
            pw.append(",");
            appendToFileWriter(pw, ds.getM14());            
            pw.append(",");
            appendToFileWriter(pw, ds.getM15());
            pw.append(",");
            appendToFileWriter(pw, ds.getM16());            
            pw.append(",");
            appendToFileWriter(pw, ds.getM17());
            pw.append(",");
            appendToFileWriter(pw, ds.getM18());
            pw.append(",");
            appendToFileWriter(pw, ds.getM19());
            pw.append(",");
            appendToFileWriter(pw, ds.getM20());            
            pw.append(",");
            appendToFileWriter(pw, ds.getM21());
            pw.append(",");
            appendToFileWriter(pw, ds.getM22());            
            pw.append(",");
            appendToFileWriter(pw, ds.getM23());            
            pw.append(",");
            appendToFileWriter(pw, ds.getM24());            
            pw.append(",");
            appendToFileWriter(pw, ds.getM25());            
            pw.append(",");
            appendToFileWriter(pw, ds.getM26());            
            pw.append(",");
            appendToFileWriter(pw, ds.getM27());
            pw.append(",");
            appendToFileWriter(pw, ds.getM28());            
            pw.append(",");
            appendToFileWriter(pw, ds.getM29());            
            pw.append(",");
            appendToFileWriter(pw, ds.getM30());            
            pw.append(",");
            appendToFileWriter(pw, ds.getM31());            
            pw.append(",");
            appendToFileWriter(pw, ds.getM32());            

            pw.append("\n");
            pw.flush();
            pw.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }
}
