package ca.ryerson.lod.metrics.semanticstuff.services;

import ca.ryerson.lod.metrics.semanticstuff.services.ModelStatistics.ModelStats;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 *
 * Metric 15. Membership of disjoint classes The number of entities are defined
 * as being members of disjoint classes
 *
 */
public class MetricFifteenService extends MetricEightService {

    private static final Logger logger = Logger.getLogger(MetricFifteenService.class);
    private List<OntClass> listOfDisjointClasses = new LinkedList<OntClass>();

    @Override
    Double calculateMetric(OntModel ontModel) {
        // TODO Auto-generated method stub
        ServiceResults counter = this.getMetric(ontModel);
        Double result = Double.valueOf(counter.getHitsCounter()) / counter.getDenominator();
        logger.info(this.getClass().getSimpleName() + " RESULT:" + result + "  hits:" + counter.getHitsCounter() + "/" + counter.getDenominator());
        cleanData();
        return (1.00 - result);
    }

    @Override
    public void cleanData() {
        super.cleanData();
        listOfDisjointClasses.clear();
    }

    @Override
    public ServiceResults getMetric(OntModel ontModel) {
        ServiceResults counter = super.getMetric(ontModel);
        int total = 0;
        //System.out.println(listOfDisjointClasses);
        List<OntResource> repeat = new LinkedList<>();
                
        for (OntClass disjointClass : listOfDisjointClasses) {
            @SuppressWarnings("unchecked")
            List<OntResource> instances = (List<OntResource>) disjointClass.listInstances().toList();
                        
            for(int i = 0; i < instances.size(); i++) {
                if (repeat.contains(instances.get(i))) {
                    total++;
                    
                } else {
                    repeat.add(instances.get(i));
                }
                
            }
            
            //total = total + instances.size();
        }
        
        //System.out.println(repeat.size());
        
        ModelStatistics modelStats = new ModelStatistics();
        ModelStats stats = modelStats.getModelStats(ontModel);
        counter.setDenominator(stats.instancesNo);
        counter.setHitsCounter(total);        
        return counter;

    }

    @Override
    ServiceResults checkOntClass(OntClass ontClass, ServiceResults counter) {
        // TODO Auto-generated method stub
        if (checkedClasses.contains(ontClass.getURI())) {
            return counter;
        }
        checkedClasses.add(ontClass.getURI());
        ExtendedIterator<OntClass> disjointClasses = ontClass.listDisjointWith();
        //List<OntResource> classInstances=  (List<OntResource>) ontClass.listInstances().toList();
        int size = disjointClasses.toList().size();
        if (size > 0) {
            if (!listOfDisjointClasses.contains(ontClass)) {
                listOfDisjointClasses.add(ontClass);
                //System.out.println(ontClass.toString());
            }
        }
        while (disjointClasses.hasNext()) {
            OntClass classToCheck = disjointClasses.next();
            if (!listOfDisjointClasses.contains(classToCheck)) {
                listOfDisjointClasses.add(classToCheck);
                //System.out.println(classToCheck.toString());
            }
        }

        return counter;
    }
    
}
