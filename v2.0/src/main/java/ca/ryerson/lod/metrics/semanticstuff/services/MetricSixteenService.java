package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.FunctionalProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Majid Sazvar
 */
/**
 *
 * Metric 16. Invalid usage of inverse-functional properties The number of
 * triples contains invalid usage of inverse-functional properties
 *
 */
public class MetricSixteenService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MetricSixteenService.class);

    @Override
    ServiceResults checkOntClass(OntClass ontClass, ServiceResults counter) {
        if (checkedClasses.contains(ontClass.getURI())) {
            return counter;
        }
        checkedClasses.add(ontClass.getURI());
        @SuppressWarnings("unchecked")
        ExtendedIterator<OntResource> classInstances = (ExtendedIterator<OntResource>) ontClass.listInstances();
        ExtendedIterator<OntProperty> classproperties = ontClass.listDeclaredProperties();

        ArrayList<FunctionalProperty> invFunctProperties = getAllInverseFunctionalPropertiesOfClass(ontClass, classproperties);

        //System.out.println(invFunctProperties.toString());
        
        while (classInstances.hasNext()) {
            OntResource ontResource = classInstances.next();
            counter = checkClassInstance(ontClass, invFunctProperties, ontResource, counter);

        }
        return counter;
    }

    public ArrayList<FunctionalProperty> getAllInverseFunctionalPropertiesOfClass(
            OntClass oC, ExtendedIterator<OntProperty> classproperties) {

        ArrayList<FunctionalProperty> dP = new ArrayList();
        while (classproperties.hasNext()) {
            OntProperty ontProp = classproperties.next();
            if (ontProp.isInverseFunctionalProperty()) {
                dP.add(ontProp.asFunctionalProperty());
            }

        }

        return dP;
    }

    ServiceResults checkClassInstance(OntClass ontClass,
            ArrayList<FunctionalProperty> invFunctProperties,
            OntResource ontResource, ServiceResults counter) {
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());
        
        List<Statement> properties = ontResource.listProperties().toList();
        if (properties != null && properties.size() > 0) {
            counter.increaseDenominator(properties.size());
        }
                
        for (FunctionalProperty invFP : invFunctProperties) {

            if (ontResource.hasProperty(invFP)) {
                
                boolean validUse = false;
                Statement statement = ontResource.getProperty(invFP);
                RDFNode object = statement.getObject();
                if (object.isResource()) {
                    
                    Resource objResource = object.asResource();
                    ResIterator subIterator = ontClass.getModel().listSubjectsWithProperty(invFP, objResource);
                    
                    validUse = true;
                    while(subIterator.hasNext()) {
                        Resource sub = subIterator.next();
                        
                        if (!sub.equals(ontResource)) {
                            validUse = false;
                        }
                    }
                }
                if (!validUse) {
                    counter.increaseHits();
                }
            }
        }

        return counter;
    }

    @Override
    ServiceResults checkClassInstance(OntClass ontClass,
            ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}
