package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.List;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 12: Ratio of unique properties 1-(The number of similar properties /
 * total numbers of properties defined in the schema of a dataset)
 *
 */
public class MetricTwelveService extends AbstractMetricsService {

    private static final Logger logger = Logger.getLogger(MetricTwelveService.class);  

    @Override
    ServiceResults checkClassInstance(OntClass ontClass,
            ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {

        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());
        return counter;
    }

    @Override
    public void cleanData() {
        super.cleanData();
        
    }

    @Override
    public ServiceResults getMetric(OntModel ontModel) {
        ServiceResults counter = super.getMetric(ontModel);

        List<OntProperty> allProperties = ontModel.listAllOntProperties().toList();

        counter.setDenominator(allProperties.size());
        for (int i = 0; i < allProperties.size(); i++) {
            OntProperty currProperty = allProperties.get(i);

            for (int j = 0; j < i; j++) {
                OntProperty prevProperty = allProperties.get(j);

                if (similarProperties(ontModel, currProperty, prevProperty)) {
                    //System.out.println(currProperty + " == " + prevProperty);
                    counter.increaseHits();

                }

            }

        }
        return counter;
    }

    private boolean similarProperties(OntModel ontModel, OntProperty property1, OntProperty property2) {

        List<Statement> property1Stmts = ontModel.listStatements(null, property1, (RDFNode) null).toList();

        List<Statement> property2Stmts = ontModel.listStatements(null, property2, (RDFNode) null).toList();

        if ((property1Stmts.size() != property2Stmts.size()) || 
                (property1Stmts.isEmpty())){
            return false;

        }

        for (Statement stmt : property1Stmts) {
            
            if (!ontModel.contains(stmt.getSubject(), property2, stmt.getObject())) {
                return false;

            }
        }
        
        for (Statement stmt : property2Stmts) {
            
            if (!ontModel.contains(stmt.getSubject(), property1, stmt.getObject())) {
                return false;

            }
        }

        return true;
    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}
