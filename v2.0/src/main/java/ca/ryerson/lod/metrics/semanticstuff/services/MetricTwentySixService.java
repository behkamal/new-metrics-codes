package ca.ryerson.lod.metrics.semanticstuff.services;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.util.HashMap;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 26. Ratio of redundant triples 1-(The number of redundant S P O /
 * total numbers of triples )
 *
 */
public class MetricTwentySixService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger
            .getLogger(MetricTwentySixService.class);

    @Override
    ServiceResults checkClassInstance(OntClass ontClass,
            ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {

        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());

        StmtIterator propertiesIt = ontResource.listProperties();
        HashMap<String, Integer> propValue = new HashMap();

        while (propertiesIt.hasNext()) {
            Statement statement = propertiesIt.next();
            Property property = statement.getPredicate();
            RDFNode object = statement.getObject();
            Object value = null;
            String propertyUri = property.getURI();

            counter.increaseDenominator();

            if (object.isLiteral()) {
                value = object.asLiteral().getValue();                

            } else {
                value = object.asResource().getLocalName();
            }
            if (value != null) {
                String key = propertyUri + "|" + value;
                if (propValue.containsKey(key)) {
                    /*
                    System.out.println(statement.getSubject() + ", "
                            + statement.getPredicate() + ", " 
                            + statement.getObject());
                    */
                    counter.increaseHits();

                } else {
                    propValue.put(key, 0);

                }
            }

        }
        return counter;

    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}
