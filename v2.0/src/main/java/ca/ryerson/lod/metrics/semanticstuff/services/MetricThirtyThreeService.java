package ca.ryerson.lod.metrics.semanticstuff.services;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.util.Map;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 33. % triples using user-defined properties / total numbers of triples
 * in a dataset )
 *
 */
public class MetricThirtyThreeService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MetricThirtyThreeService.class);
    private Map<String, String> nsPrefixMap;
    
    @Override
    ServiceResults checkClassInstance(OntClass ontClass, ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());

        StmtIterator hasproperties = ontResource.listProperties();
        while (hasproperties.hasNext()) {

            Statement statement = hasproperties.next();
            Property property = statement.getPredicate();

            counter.increaseDenominator();
            
            if (!nsPrefixMap.containsValue(property.getNameSpace())) {
                //System.out.println(property.getNameSpace());
                counter.increaseHits();
            }

        }
        return counter;

    }   

    @Override
    Double calculateMetric(OntModel ontModel) {                
        nsPrefixMap = ontModel.getNsPrefixMap();
        nsPrefixMap.remove("");
        
        Double result = super.calculateMetric(ontModel);
        return (result);
    }
}
