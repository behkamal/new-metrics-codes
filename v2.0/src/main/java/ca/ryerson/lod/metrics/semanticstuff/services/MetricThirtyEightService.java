package ca.ryerson.lod.metrics.semanticstuff.services;

import com.hp.hpl.jena.ontology.ObjectProperty;
import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.util.ArrayList;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 38. % number of triples with predicate as a resource / 
 * n * (n - 1) / 2 "n = number of resources in a dataset"
 *
 */
public class MetricThirtyEightService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MetricThirtyEightService.class);
    ArrayList<String> objectProperties = null;

    @Override
    ServiceResults checkClassInstance(OntClass ontClass, ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());

        StmtIterator hasproperties = ontResource.listProperties();
        while (hasproperties.hasNext()) {

            Statement statement = hasproperties.next();
            RDFNode object = statement.getObject();
            
            if (object.isResource()) {
                
                counter.increaseHits();
            }

        }
        return counter;

    }    
    
    @Override
    public ServiceResults getMetric(OntModel ontModel) {
        ServiceResults counter = new ServiceResults();

        int numberOfResources = ontModel.listResourcesWithProperty(null).toList().size();
        //System.out.println(numberOfResources);
        
        numberOfResources = numberOfResources * (numberOfResources - 1) / 2;
                
        counter.setDenominator(numberOfResources);
        
        return counter;
    }

    @Override
    Double calculateMetric(OntModel ontModel) {       
        Double result = super.calculateMetric(ontModel);
        return (result);
    }
}
