package ca.ryerson.lod.metrics.semanticstuff.services;

import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.util.List;

/**
 * @author Majid Sazvar
 */
/**
 *
 * Metric 17. Redefined existing properties 1-(The number of triples using
 * similar properties / total numbers of triples)
 *
 */
public class MetricSeventeenService extends AbstractMetricsService {

    private static final Logger logger = Logger.getLogger(MetricSeventeenService.class);

    @Override
    ServiceResults checkClassInstance(OntClass ontClass,
            ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {

        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());
        
        int triplesSize = ontResource.listProperties().toList().size();
        
        counter.increaseDenominator(triplesSize);
        
        return counter;
    }

    @Override
    public void cleanData() {
        super.cleanData();

    }

    @Override
    public ServiceResults getMetric(OntModel ontModel) {
        ServiceResults counter = super.getMetric(ontModel);

        List<OntProperty> allProperties = ontModel.listAllOntProperties().toList();
        
        for (int i = 0; i < allProperties.size(); i++) {
            OntProperty currProperty = allProperties.get(i);

            for (int j = 0; j < i; j++) {
                OntProperty prevProperty = allProperties.get(j);

                int hits = countTriplesWithSimilarProperties(ontModel, currProperty, prevProperty);

                if (hits > 0) {
                    //System.out.println(currProperty + " == " + prevProperty + " " + hits);
                }
                counter.increaseHits(hits);

            }

        }
        return counter;
    }

    private int countTriplesWithSimilarProperties(OntModel ontModel, OntProperty property1, OntProperty property2) {

        List<Statement> property1Stmts = ontModel.listStatements(null, property1, (RDFNode) null).toList();

        List<Statement> property2Stmts = ontModel.listStatements(null, property2, (RDFNode) null).toList();

        if ((property1Stmts.size() != property2Stmts.size())
                || (property1Stmts.isEmpty())) {
            return 0;

        }

        for (Statement stmt : property1Stmts) {

            if (!ontModel.contains(stmt.getSubject(), property2, stmt.getObject())) {
                return 0;

            }
        }
        
        for (Statement stmt : property2Stmts) {

            if (!ontModel.contains(stmt.getSubject(), property1, stmt.getObject())) {
                return 0;

            }
        }

        return property1Stmts.size();
    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}