package ca.ryerson.lod.metrics.semanticstuff.services;

import ca.ryerson.lod.metrics.semanticstuff.services.ModelStatistics.ModelStats;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.OWL;

/**
 * @author Majid Sazvar
 */
/**
 *
 * Metric 23. Using disjoint properties 
 *
 */
public class MetricTwentyThreeService extends MetricEightService {

    private static final Logger logger = Logger.getLogger(MetricTwentyThreeService.class);
    private List<OntProperty> listOfDisjointProperties = new LinkedList<OntProperty>();

    @Override
    Double calculateMetric(OntModel ontModel) {
        // TODO Auto-generated method stub
        ServiceResults counter = this.getMetric(ontModel);
        Double result = Double.valueOf(counter.getHitsCounter()) / counter.getDenominator();
        logger.info(this.getClass().getSimpleName() + " RESULT:" + result + "  hits:" + counter.getHitsCounter() + "/" + counter.getDenominator());
        cleanData();
        return (1.00 - result);
    }

    @Override
    public void cleanData() {
        super.cleanData();
        listOfDisjointProperties.clear();
    }

    @Override
    public ServiceResults getMetric(OntModel ontModel) {
        ServiceResults counter = super.getMetric(ontModel);
        int total = 0;
        //System.out.println(listOfDisjointProperties);
        List<Resource> repeat = new LinkedList<>();

        for (OntProperty disjointProperty : listOfDisjointProperties) {
            @SuppressWarnings("unchecked")
            List<Resource> instances = disjointProperty.getModel().listSubjectsWithProperty(disjointProperty).toList();                      

            for (int i = 0; i < instances.size(); i++) {
                if (repeat.contains(instances.get(i))) {
                    total++;
                    

                } else {
                    repeat.add(instances.get(i));
                }

            }

            //total = total + instances.size();
        }

        //System.out.println(repeat.size());

        ModelStatistics modelStats = new ModelStatistics();
        ModelStats stats = modelStats.getModelStats(ontModel);
        counter.setDenominator(stats.instancesNo);
        counter.setHitsCounter(total);
        return counter;

    }

    @Override
    ServiceResults checkOntClass(OntClass ontClass, ServiceResults counter) {
        // TODO Auto-generated method stub
        if (checkedClasses.contains(ontClass.getURI())) {
            return counter;
        }
        checkedClasses.add(ontClass.getURI());

        ExtendedIterator<OntProperty> disjointProperties = ontClass.listDeclaredProperties();

        while (disjointProperties.hasNext()) {
            OntProperty propertyToCheck = disjointProperties.next();

            NodeIterator ite = propertyToCheck.getOntModel().listObjectsOfProperty(propertyToCheck, OWL.disjointWith);           

            boolean added = false;
            while (ite.hasNext()) {
                RDFNode node = ite.next();
                OntProperty property = node.as(OntProperty.class);
                
                added = true;

                if (!listOfDisjointProperties.contains(property)) {
                    listOfDisjointProperties.add(property);                    
                    //System.out.println(property.toString());
                }
            }
            
            if (added) {
                if (!listOfDisjointProperties.contains(propertyToCheck)) {
                    listOfDisjointProperties.add(propertyToCheck);
                    //System.out.println(propertyToCheck.toString());
                }
            }
        }

        return counter;
    }
}
