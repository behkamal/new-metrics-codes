package ca.ryerson.lod.metrics.semanticstuff.services;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.util.ArrayList;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 39. % number of resources that appeared as subject and object / 
 * number of resources that appeared only as object
 *
 */
public class MetricThirtyNineService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MetricThirtyNineService.class);

    @Override
    ServiceResults checkClassInstance(OntClass ontClass, ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());

        return counter;

    }    
    
    @Override
    public ServiceResults getMetric(OntModel ontModel) {
        ServiceResults counter = new ServiceResults();

        NodeIterator niter = ontModel.listObjects();
        ArrayList<String> resourcesAsObject = new ArrayList();
        
        while(niter.hasNext()) {
            RDFNode node = niter.next();
            
            if (node.isResource()) {                
                Resource res = node.asResource();
                
                if (!resourcesAsObject.contains(res.getURI())) {
                    resourcesAsObject.add(res.getURI());
                    
                }
            }
        
        }
        counter.setDenominator(resourcesAsObject.size());
        
        ResIterator riter = ontModel.listSubjects();
        
        while(riter.hasNext()) {
            Resource res = riter.next();
            
            if(resourcesAsObject.contains(res.getURI())) {
                counter.increaseHits();
                resourcesAsObject.remove(res.getURI());
            }
        }                        
        
        return counter;
    }

    @Override
    Double calculateMetric(OntModel ontModel) {       
        Double result = super.calculateMetric(ontModel);
        return (result);
    }
}
