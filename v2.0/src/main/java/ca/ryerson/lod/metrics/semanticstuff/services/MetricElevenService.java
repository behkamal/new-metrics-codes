package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
/**
 * Metric 11: Ratio of unique classes 1-(The number of unique classes / total
 * numbers of classes defined in the schema of a dataset )
 */
public class MetricElevenService extends AbstractMetricsService {

    private static final Logger logger = Logger.getLogger(MetricElevenService.class);
    private List<OntClass> classes = new LinkedList<OntClass>();

    @Override
    ServiceResults checkClassInstance(OntClass ontClass,
            ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {
        // TODO Auto-generated method stub
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());
        return counter;
    }

    @Override
    public void cleanData() {
        super.cleanData();
        classes.clear();
    }

    @Override
    public ServiceResults getMetric(OntModel ontModel) {
        ServiceResults counter = super.getMetric(ontModel);
        counter.setDenominator(classes.size());
        List<OntClass> hasPairClasses = new LinkedList<OntClass>();
        for (OntClass ontClass : classes) {
            boolean haveSameClass = false;
            ExtendedIterator<OntProperty> classproperties = ontClass.listDeclaredProperties();
            List<String> properties = new LinkedList<String>();
            while (classproperties.hasNext()) {
                properties.add(classproperties.next().getURI());
            }
            for (OntClass otherClass : classes) {
                if (!otherClass.equals(ontClass)) {
                    haveSameClass = compareProperties(properties, otherClass);
                }
            }
            //System.out.println("Class: " + ontClass);
            if (!haveSameClass) {
                //System.out.println("hasPairClasses: " + hasPairClasses);
                if (!hasPairClasses.contains(ontClass)) {
                    hasPairClasses.add(ontClass);
                }
            }
        }
        counter.setHitsCounter(hasPairClasses.size());
        return counter;
    }

    private boolean compareProperties(List<String> properties, OntClass ontClass2) {
        ExtendedIterator<OntProperty> secondclassproperties = ontClass2.listDeclaredProperties();
        while (secondclassproperties.hasNext()) {
            OntProperty subproperty = secondclassproperties.next();
            String propUri = subproperty.getURI();
            if (!properties.contains(propUri)) {
                return false;
            }
        }
        return true;
    }

    @Override
    void iterateOntClass(OntClass ontClass, ServiceResults counter) {
        if (!classes.contains(ontClass)) {
            //logger.info("addOntClass:" + ontClass.getLocalName());
            classes.add(ontClass);
            ExtendedIterator<OntClass> subclasses = ontClass.listSubClasses();
            while (subclasses.hasNext()) {
                OntClass subclass = subclasses.next();
                iterateOntClass(subclass, counter);
            }
        }

    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}
