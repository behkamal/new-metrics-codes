package ca.ryerson.lod.metrics.semanticstuff.services;

import com.hp.hpl.jena.ontology.ObjectProperty;
import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.util.ArrayList;
import java.util.Map;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 37. % number of triples with object property as predicate and external
 * object / total numbers of triples using object properties )
 *
 */
public class MetricThirtySevenService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MetricThirtySevenService.class);
    private ArrayList<String> objectProperties = null;
    private Map<String, String> nsPrefixMap;
    
    @Override
    ServiceResults checkClassInstance(OntClass ontClass, ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());

        StmtIterator hasproperties = ontResource.listProperties();
        while (hasproperties.hasNext()) {

            Statement statement = hasproperties.next();
            Property predicate = statement.getPredicate();

            if (objectProperties.contains(predicate.getURI())) {
                counter.increaseDenominator();                                

                Resource object = statement.getObject().asResource();
                
                if (nsPrefixMap.containsValue(object.getNameSpace())) {
                    //System.out.println(object.getNameSpace());
                    counter.increaseHits();
                }
            }

        }
        return counter;

    }

    private void getObjectProperties(OntModel ontModel) {
        ExtendedIterator<ObjectProperty> iter = ontModel.listObjectProperties();
        objectProperties = new ArrayList();

        while (iter.hasNext()) {
            ObjectProperty objectProperty = iter.next();
            objectProperties.add(objectProperty.getURI());
        }

    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        nsPrefixMap = ontModel.getNsPrefixMap();
        nsPrefixMap.remove("");
        //System.out.println(nsPrefixMap.toString());
        getObjectProperties(ontModel);

        Double result = super.calculateMetric(ontModel);
        return (result);
    }
}
