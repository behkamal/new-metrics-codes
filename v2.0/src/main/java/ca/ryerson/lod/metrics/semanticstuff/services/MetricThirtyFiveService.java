package ca.ryerson.lod.metrics.semanticstuff.services;

import com.hp.hpl.jena.ontology.ObjectProperty;
import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.util.ArrayList;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 35. % number of triples using object property / total numbers of
 * triples in a dataset )
 *
 */
public class MetricThirtyFiveService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MetricThirtyFiveService.class);
    ArrayList<String> objectProperties = null;

    @Override
    ServiceResults checkClassInstance(OntClass ontClass, ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());

        StmtIterator hasproperties = ontResource.listProperties();
        while (hasproperties.hasNext()) {

            Statement statement = hasproperties.next();
            Property predicate = statement.getPredicate();
            
            counter.increaseDenominator();

            if (objectProperties.contains(predicate.getURI())) {
                //System.out.println(predicate);
                counter.increaseHits();
            }

        }
        return counter;

    }
    
    private void getObjectProperties(OntModel ontModel) {
        ExtendedIterator<ObjectProperty> iter = ontModel.listObjectProperties();
        objectProperties = new ArrayList();

        while (iter.hasNext()) {
            ObjectProperty objectProperty = iter.next();
            objectProperties.add(objectProperty.getURI());
        }

    }          

    @Override
    Double calculateMetric(OntModel ontModel) {
        getObjectProperties(ontModel);
        
        Double result = super.calculateMetric(ontModel);
        return (result);
    }
}
