package ca.ryerson.lod.metrics.semanticstuff.services;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 36. % number of imported triples / total numbers of triples in a
 * dataset )
 *
 */
public class MetricThirtySixService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MetricThirtySixService.class);
    
    private Model baseModel = null;

    @Override
    ServiceResults checkClassInstance(OntClass ontClass, ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());

        StmtIterator hasproperties = ontResource.listProperties();
        while (hasproperties.hasNext()) {

            Statement statement = hasproperties.next();
                        
            counter.increaseDenominator();

            if (!baseModel.contains(statement)) {
                //System.out.println(statement);
                counter.increaseHits();
            }

        }
        return counter;

    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        baseModel = ontModel.getBaseModel();
        Double result = super.calculateMetric(ontModel);
        return (result);
    }
}
