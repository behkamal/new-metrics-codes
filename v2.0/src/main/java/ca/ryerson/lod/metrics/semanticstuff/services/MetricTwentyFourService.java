package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.List;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Majid Sazvar
 */
/**
 *
 * Metric 24. Unused classes (classes defined in the schema but not used in the
 * dataset)
 *
 */
public class MetricTwentyFourService extends AbstractMetricsService {

    private static final Logger logger = Logger.getLogger(MetricTwentyFourService.class);
    private List<OntClass> classList;

    @Override
    public ServiceResults getMetric(OntModel ontModel) {

        classList = ontModel.listNamedClasses().toList();
        int classCount = classList.size();

        ServiceResults counter = super.getMetric(ontModel);

        //System.out.println(classList);

        counter.setDenominator(classCount);
        counter.setHitsCounter(classList.size());

        return counter;

    }

    @Override
    ServiceResults checkClassInstance(OntClass ontClass,
            ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {

        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());

        List<Resource> rdfTypes = ontResource.listRDFTypes(true).toList();

        //System.out.println(classList);
        for (Resource rdfType : rdfTypes) {
            OntResource rdfTypeOnt = rdfType.as(OntResource.class);

            //JenaUtil.asOntResource(rdfType);

            if (rdfTypeOnt.isClass()) {

                classList.remove(rdfTypeOnt.asClass());
            }

        }

        return counter;
    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}
