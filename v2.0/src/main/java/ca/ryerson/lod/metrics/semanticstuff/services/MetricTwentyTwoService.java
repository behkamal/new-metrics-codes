package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 22. % resources using undefined classes 1-(The number of
 * entities using undefined classes / total numbers of entities in a
 * dataset )
 *
 */
public class MetricTwentyTwoService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger
            .getLogger(MetricTwentyTwoService.class);

    @Override
    public ServiceResults getMetric(OntModel ontModel) {
        ServiceResults counter = new ServiceResults();

        ExtendedIterator<OntClass> classes = ontModel.listNamedClasses();
        List<String> classList = new LinkedList<>();
        while (classes.hasNext()) {
            OntClass rClass = classes.next();
            classList.add(rClass.getURI());
            
        }
        
        classes = ontModel.listNamedClasses();
        while (classes.hasNext()) {
            OntClass rClass = classes.next();
            counter = checkOntClass(rClass, classList, counter);
        }

        return counter;
    }
        
    ServiceResults checkOntClass(OntClass ontClass, List<String> classList, ServiceResults counter) {
        //System.out.println("checkOntClass:" + ontClass.getURI());
        if (checkedClasses.contains(ontClass.getURI())) {
            return counter;
        }
        checkedClasses.add(ontClass.getURI());
        ExtendedIterator<OntResource> classInstances = (ExtendedIterator<OntResource>) ontClass.listInstances();
        
        while (classInstances.hasNext()) {
            OntResource ontResource = classInstances.next();            

            counter = checkClassInstance(ontClass, classList, ontResource, counter);

        }
        return counter;
    }
    
    ServiceResults checkClassInstance(OntClass ontClass,
            List<String> classList,
            OntResource ontResource, ServiceResults counter) {

        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());

        List<Resource> rdfTypes = ontResource.listRDFTypes(true).toList();        

        boolean match = false;
        //System.out.println(classList);
        for (Resource rdfType : rdfTypes) {
            OntResource rdfTypeOnt = rdfType.as(OntResource.class);

            
            if (!classList.contains(rdfTypeOnt.getURI())) {
                System.out.println("not contains: " + rdfTypeOnt.getURI());
                match = true;
            }


        }

        if (match == true) {
            counter.increaseHits();
        }
        counter.increaseDenominator();
        return counter;
    }
    
    @Override
    ServiceResults checkClassInstance(OntClass ontClass,
            ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {

        return null;
    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}
