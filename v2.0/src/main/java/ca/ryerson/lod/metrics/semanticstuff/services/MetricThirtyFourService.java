package ca.ryerson.lod.metrics.semanticstuff.services;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.RDFS;
import java.util.ArrayList;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 34. % number of triples using misplaced classes and properties / total
 * numbers of triples in a dataset
 *
 */
public class MetricThirtyFourService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MetricThirtyFourService.class);
    ArrayList<String> objectProperties = null;
    private OntModel ontModel;

    @Override
    ServiceResults checkClassInstance(OntClass ontClass, ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());

        StmtIterator hasproperties = ontResource.listProperties();
        while (hasproperties.hasNext()) {

            counter.increaseDenominator();

            Statement statement = hasproperties.next();

            String subject = statement.getSubject().getURI();
            Property predicate = statement.getPredicate();

            String object = "";
            if (statement.getObject().isResource()) {
                object = statement.getObject().asResource().getURI();
            }

            boolean misplaced = false;

            if ((predicate.equals(RDFS.domain)) || (predicate.equals(RDFS.range))) {
                misplaced = !checkIsProperty(subject) || !checkIsClass(object);

            } else if (predicate.equals(RDFS.subClassOf)) {
                misplaced = !checkIsClass(subject) || !checkIsClass(object);
                
            } else if (predicate.equals(RDFS.subPropertyOf)) {
                misplaced = !checkIsProperty(subject) || !checkIsProperty(object);
                        
            //} else if (predicate.equals(RDF.type)) {
            //    misplaced = !checkIsIndividual(subject) || !checkIsClass(object);
                
            } else if (!predicate.isProperty()) {
                misplaced = true;
            }

            if (misplaced) {
                System.out.println(statement);

                counter.increaseHits();
            }

        }

        return counter;

    }

    private boolean checkIsClass(String uri) {
        return ontModel.getOntClass(uri) != null ? true : false;
    }

    private boolean checkIsProperty(String uri) {
        return ontModel.getOntProperty(uri) != null ? true : false;
    }

    private boolean checkIsIndividual(String uri) {
        return ontModel.getIndividual(uri) != null ? true : false;
    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        this.ontModel = ontModel;
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}
