package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Majid Sazvar
 */
/**
 *
 * Metric 18. Heterogeneous data types: The number of heterogeneous data types in
 * the schema
 *
 */
public class MetricEighteenService extends AbstractMetricsService {

    private static final Logger logger = Logger.getLogger(MetricEighteenService.class);
    List<String> results = new LinkedList<String>();

    @Override
    ServiceResults checkOntClass(OntClass ontClass, ServiceResults counter) {
        if (checkedClasses.contains(ontClass.getURI())) {
            return counter;
        }
        checkedClasses.add(ontClass.getURI());
        //StmtIterator properties=ontClass.listProperties();
        ExtendedIterator<OntProperty> decProps = ontClass.listDeclaredProperties();
        while (decProps.hasNext()) {
            OntProperty ontProp = decProps.next();
            if (ontProp.isDatatypeProperty()) {
                OntResource range = ontProp.getRange();
                if (range != null) {
                    String rangeUri = range.getURI();
                    if (!results.contains(rangeUri)) {

                        //System.out.println(ontProp + ", " + range);

                        results.add(rangeUri);
                        counter.increaseHits();
                    }
                }
            }
        }

        return counter;
    }

    @Override
    public void cleanData() {
        super.cleanData();
        results.clear();
    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        // TODO Auto-generated method stub
        ServiceResults counter = this.getMetric(ontModel);
        Double result = Double.valueOf(counter.getHitsCounter());
        logger.info(this.getClass().getSimpleName() + " RESULT:" + result + "  hits:" + counter.getHitsCounter() + "/" + counter.getDenominator());
        cleanData();
        return result;
    }

    @Override
    ServiceResults checkClassInstance(OntClass ontClass,
            ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {
        // TODO Auto-generated method stub
        return null;
    }
}
