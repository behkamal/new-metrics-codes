package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 27. % The average of missing data values ratio: 1-(The sum of missing
 * values ratio in different instances / total numbers of instances in a
 * dataset)
 *
 */
public class MetricTwentySevenService extends AbstractMetricsService {

    @SuppressWarnings("unused")
    private static final Logger logger = Logger.getLogger(MetricTwentySevenService.class);

    @Override
    ServiceResults checkClassInstance(OntClass ontClass, ExtendedIterator<OntProperty> classproperties, OntResource ontResource,
            ServiceResults counter) {
        if (checkedInstances.contains(ontResource.getURI())) {
            return counter;
        }
        checkedInstances.add(ontResource.getURI());
        StmtIterator hasproperties = ontResource.listProperties();
        List<String> hasPropertiesList = new LinkedList<String>();
        while (hasproperties.hasNext()) {

            Statement statement = hasproperties.next();
            hasPropertiesList.add(statement.getPredicate().getURI());

        }
        //System.out.println("List size: " + classproperties.toSet().size());
        int denominator = 0;
        int ihits = 0;
        while (classproperties.hasNext()) {
            OntProperty subproperty = classproperties.next();

            denominator++;

            if (!hasPropertiesList.contains(subproperty.getURI())) {
                //System.out.println("Missed: " + subproperty.toString());
                ihits++;

            }
        }

        counter.increaseDenominator();
        float fhits = ((float) ihits / denominator) * 100;
        counter.increaseHits(Math.round(fhits));

        return counter;
    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        result = result / 100.0;
        return (1.00 - result);
    }
}
