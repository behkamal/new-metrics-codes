package ca.ryerson.lod.metrics.semanticstuff.services;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Arrays;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.search.spell.PlainTextDictionary;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.search.spell.StringDistance;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Majid Sazvar
 */
/**
 * Metric 30. % property names contains misspelling 1-(The number of properties
 * contain misspelling in their names / total numbers of properties defined in 
 * schema) 
 * 
 */
public class MetricThirtyService extends AbstractMetricsService {

    private static Logger logger = Logger.getLogger(MetricThirtyService.class
            .getName());
    private Set<String> indexedLanguages = new HashSet<String>();

    @Override
    public void cleanData() {
        super.cleanData();
        //indexedLanguages.clear();	
    }
    private long maxMemorytUsage = 128 * 1024 * 1024; // 128 megabytes
    private Map<String, MemoryAwareSpellChecker> spellcheckersCache = new Hashtable<String, MemoryAwareSpellChecker>();
    @SuppressWarnings("serial")
    private Map<String, MemoryAwareSpellChecker> inMemorySpellcheckersCache = new LinkedHashMap<String, MemoryAwareSpellChecker>(
            2, 0.75f, true) {
        @Override
        protected boolean removeEldestEntry(
                Map.Entry<String, MemoryAwareSpellChecker> eldest) {
            return getSizeInBytes() > maxMemorytUsage;
        }

        public long getSizeInBytes() {
            long size = 0;
            for (MemoryAwareSpellChecker spellChecker : values()) {
                size += spellChecker.getIndexSize();
            }
            return size;
        }
    };
   
    protected List<String> findSuggestions(String word, String lang,
            int maxSuggestions) throws SpellCheckException {
        SpellChecker checker = (SpellChecker) getChecker(lang);
        try {
            String[] suggestions = checker.suggestSimilar(word, maxSuggestions);
            return Arrays.asList(suggestions);
        } catch (IOException e) {
            logger.info("Failed to find suggestions" + e);
            throw new SpellCheckException("Failed to find suggestions", e);
        }
    }

    private MemoryAwareSpellChecker loadSpellChecker(final String lang)
            throws SpellCheckException {
        MemoryAwareSpellChecker checker = null;
        if (!indexedLanguages.contains(lang)) {
            indexedLanguages.add(lang);
            checker = reindexSpellchecker(lang);
        } else {
            try {
                checker = new MemoryAwareSpellChecker(
                        getSpellCheckerDirectory(lang));
            } catch (IOException e) {
                throw new SpellCheckException("Failed to create index", e);
            }
        }
        return checker;
    }

    private Directory getSpellCheckerDirectory(String language)
            throws IOException {
        String path = "./spellchecker/lucene/" + language;
        return FSDirectory.open(new File(path));
        //return FSDirectory.getDirectory(path);
    }

    protected List<File> getDictionaryFiles(String language)
            throws SpellCheckException {
        String pathToDictionaries = "data/dictionaries/lucene";
        File dictionariesDir = new File(pathToDictionaries);
        List<File> langDictionaries = getDictionaryFiles(language,
                dictionariesDir, language);
        if (langDictionaries.size() == 0) {
            logger.info(
                    "There is no dictionaries for the language=" + language);
        }
        List<File> globalDictionaries = getDictionaryFiles("global",
                dictionariesDir, "global");

        List<File> dictionariesFiles = new ArrayList<File>();
        dictionariesFiles.addAll(langDictionaries);
        dictionariesFiles.addAll(globalDictionaries);
        return dictionariesFiles;
    }

    private List<File> getDictionaryFiles(final String lang,
            File dictionariesDir, final String prefix)
            throws SpellCheckException {

        File languageDictionary = new File(dictionariesDir, lang);
        FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (pathname.isFile()) {
                    return pathname.getName().startsWith(prefix);
                }
                return false;
            }
        };
        File[] languageDictionaries = languageDictionary.listFiles(filter);


        List<File> dictionaries = new ArrayList<File>();
        if (languageDictionaries != null) {
            dictionaries.addAll(Arrays.asList(languageDictionaries));
        }
        return dictionaries;
    }

    private MemoryAwareSpellChecker reindexSpellchecker(String lang)
            throws SpellCheckException {
        MemoryAwareSpellChecker checker;
        List<File> dictionariesFiles = getDictionaryFiles(lang);
        try {
            checker = new MemoryAwareSpellChecker(
                    getSpellCheckerDirectory(lang));
            checker.clearIndex();
        } catch (IOException e) {
            throw new SpellCheckException("Failed to create index", e);
        }

        for (File dictionariesFile : dictionariesFiles) {
            try {
                checker.indexDictionary(new PlainTextDictionary(
                        dictionariesFile));
            } catch (IOException e) {
                logger.info("Failed to index dictionary "
                        + dictionariesFile.getAbsolutePath());

                throw new SpellCheckException("Failed to index dictionary "
                        + dictionariesFile.getAbsolutePath(), e);
            }
        }
        return checker;
    }

    protected Object getChecker(String lang) throws SpellCheckException {
        MemoryAwareSpellChecker cachedChecker = inMemorySpellcheckersCache
                .get(lang);
        if (cachedChecker == null) {
            MemoryAwareSpellChecker diskSpellChecker = spellcheckersCache
                    .get(lang);
            if (diskSpellChecker == null) {
                diskSpellChecker = loadSpellChecker(lang);
                spellcheckersCache.put(lang, diskSpellChecker);
            }

            try {
                cachedChecker = new MemoryAwareSpellChecker(new RAMDirectory(
                        diskSpellChecker.getSpellIndex()));
                inMemorySpellcheckersCache.put(lang, cachedChecker);
            } catch (IOException e) {
                logger.info("Failed to read index");
                throw new SpellCheckException("Failed to read index", e);
            }
        }

        return cachedChecker;
    }

    public boolean checkSpelling(String value, String lang) {


        String[] words =
                value.split("\\s+");
        boolean hasMisspellingValues = false;
        List<String> wordsList = new LinkedList<String>();
        for (int i = 0; i < words.length; i++) {

            String stringWord = words[i];

            //.replaceAll(".", ""); if(stringWord.endsWith(".")){
            stringWord = stringWord.replace(".", "");
            wordsList.add(stringWord);
        }
        try {

            List<String> misspelledWordsList = this.findMisspelledWords(wordsList.listIterator(), lang);
            if (misspelledWordsList.size() > 0) {
                hasMisspellingValues = true;
            }

        } catch (SpellCheckException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return hasMisspellingValues;
    }

    protected List<String> findMisspelledWords(
            Iterator<String> checkedWordsIterator, String lang)
            throws SpellCheckException {
        List<String> misspelledWordsList = new ArrayList<String>();
        SpellChecker checker = (SpellChecker) getChecker(lang);
        try {
            while (checkedWordsIterator.hasNext()) {
                String word = checkedWordsIterator.next();
                if (!word.equals("") && !checker.exist(word)) {
                    misspelledWordsList.add(word);
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to find misspelled words", e);
            throw new SpellCheckException("Failed to find misspelled words", e);
        }
        return misspelledWordsList;
    }

   @Override
    ServiceResults checkClassInstance(OntClass ontClass,
            ExtendedIterator<OntProperty> classproperties,
            OntResource ontResource, ServiceResults counter) {

        return null;
    }

    @Override
    public ServiceResults getMetric(OntModel ontModel) {
        ServiceResults counter = new ServiceResults();

        List<OntClass> allClasses = ontModel.listNamedClasses().toList();

        counter.increaseDenominator(allClasses.size());

        String language = "en";
        for (OntClass cls : allClasses) {
            
            String className = cls.getLocalName();
            
            String parts[] = className.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");            
            
            boolean notmatch = false;
            
            for (String word : parts) {                

                if (checkSpelling(word.toLowerCase(), language)) {
                    
                    //System.out.println(word);
                                    
                    notmatch = true;
                    break;
                }
                
            }
            
            if(notmatch) {
                
                //System.out.println("Not match: " + className);
                counter.increaseHits(); 
                
            } else {
                //System.out.println("Match: " + className);
                
            }

        }

        return counter;
    }

    private class MemoryAwareSpellChecker extends SpellChecker {

        Directory _spellIndex;

        private MemoryAwareSpellChecker(Directory spellIndex)
                throws IOException {
            super(spellIndex);
            _spellIndex = spellIndex;
        }

        private MemoryAwareSpellChecker(Directory spellIndex, StringDistance sd)
                throws IOException {
            super(spellIndex, sd);
            _spellIndex = spellIndex;
        }

        long getIndexSize() {
            if (_spellIndex instanceof RAMDirectory) {
                return ((RAMDirectory) _spellIndex).sizeInBytes();
            }
            return 0;
        }

        Directory getSpellIndex() {
            return _spellIndex;
        }
    }

    @Override
    Double calculateMetric(OntModel ontModel) {
        Double result = super.calculateMetric(ontModel);
        return (1.00 - result);
    }
}
