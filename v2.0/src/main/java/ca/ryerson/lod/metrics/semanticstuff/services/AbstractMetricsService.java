package ca.ryerson.lod.metrics.semanticstuff.services;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import ca.ryerson.lod.metrics.semanticstuff.utils.ClassUtil;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

/**
 * @author Zoran Jeremic 2013-07-18
 */
public abstract class AbstractMetricsService {

    private static final Logger logger = Logger.getLogger(AbstractMetricsService.class);
    protected List<String> checkedClasses = new LinkedList<String>();
    public List<String> checkedInstances = new LinkedList<String>();

    public ServiceResults getMetric(OntModel ontModel) {
        ServiceResults counter = new ServiceResults();

        ontModel.setStrictMode(false);
        @SuppressWarnings("rawtypes")
        /*
         Iterator rootClasses = ClassUtil.rootClasses(ontModel);
         //System.out.println("rootClasses:");
         if (rootClasses != null) {
         while (rootClasses.hasNext()) {
         OntClass rClass = (OntClass) rootClasses.next();
         iterateOntClass(rClass, counter);
         }
         } else {
         */        
        ExtendedIterator<OntClass> classes = ontModel.listNamedClasses();
        while (classes.hasNext()) {
            OntClass rClass = classes.next();
            //System.out.println(rClass.getURI());
            counter = checkOntClass(rClass, counter);
        }
        //}

        return counter;
    }

    void iterateOntClass(OntClass ontClass, ServiceResults counter) {

        counter = checkOntClass(ontClass, counter);
        ExtendedIterator<OntClass> subclasses = ontClass.listSubClasses();
        while (subclasses.hasNext()) {
            OntClass subclass = subclasses.next();
            iterateOntClass(subclass, counter);
        }


    }

    ServiceResults checkOntClass(OntClass ontClass, ServiceResults counter) {
        //System.out.println("checkOntClass:" + ontClass.getURI());
        if (checkedClasses.contains(ontClass.getURI())) {
            return counter;
        }
        checkedClasses.add(ontClass.getURI());
        ExtendedIterator<OntResource> classInstances = (ExtendedIterator<OntResource>) ontClass.listInstances();

        ExtendedIterator<OntProperty> classproperties = ontClass.listDeclaredProperties();

        while (classInstances.hasNext()) {
            OntResource ontResource = classInstances.next();

            counter = checkClassInstance(ontClass, classproperties, ontResource, counter);

            classproperties = ontClass.listDeclaredProperties();

        }

        return counter;
    }

    abstract ServiceResults checkClassInstance(OntClass ontClass, ExtendedIterator<OntProperty> classproperties, OntResource ontResource,
            ServiceResults counter);

    Double calculateMetric(OntModel ontModel) {
        // TODO Auto-generated method stub
        logger.info(this.getClass().getSimpleName() + " started metric...");
        ServiceResults counter = this.getMetric(ontModel);
        if (counter.getDenominator() == 0 || counter.getHitsCounter() == 0) {
            logger.info(this.getClass().getSimpleName() + " RESULT:" + 0.0 + "  hits:" + counter.getHitsCounter() + "/" + counter.getDenominator());
            return 0.0;
        }

        Double result = ((double) counter.getHitsCounter() / (double) counter.getDenominator());
        logger.info(this.getClass().getSimpleName() + " RESULT:" + result + "  hits:" + counter.getHitsCounter() + "/" + counter.getDenominator());
        cleanData();
        return result;
    }

    public void cleanData() {
        checkedClasses.clear();
        checkedInstances.clear();
    }
}
