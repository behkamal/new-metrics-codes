package ca.ryerson.lod.metrics.semanticstuff.rdfpersistance;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.junit.Test;

/**
 * @author Zoran Jeremic 2013-07-26
 */
public class DataSetsTest {

	@Test
	public void testGetDataSetsFiles() {
		 DataSets ds=new DataSets();
		List<String> files= ds.getDataSetsFiles("ds2", new File("data"));
		System.out.println("files number:"+files.size());
		for(String file:files){
			System.out.println(file);
		}
	}

}
